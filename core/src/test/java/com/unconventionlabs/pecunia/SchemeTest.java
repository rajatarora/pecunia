package com.unconventionlabs.pecunia;

import com.unconventionlabs.pecunia.repo.SchemeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SchemeTest {

  @Autowired
  private TestEntityManager testEntityManager;

  @Autowired
  SchemeRepository schemeRepository;

  @Test
  public void testEmptyRepository() {
    assertThat(schemeRepository.findAll()).isEmpty();
  }

  @Test
  public void contextLoads() {
  }

  @SpringBootApplication
  static class TestConfiguration {
  }

}
