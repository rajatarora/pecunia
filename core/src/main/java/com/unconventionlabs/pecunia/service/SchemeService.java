package com.unconventionlabs.pecunia.service;

import com.unconventionlabs.pecunia.exceptions.model.scheme.NoSchemeFound;
import com.unconventionlabs.pecunia.exceptions.model.scheme.SchemeAlreadyExists;
import com.unconventionlabs.pecunia.model.Scheme;
import com.unconventionlabs.pecunia.repo.SchemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SchemeService {

  private final SchemeRepository schemeRepository;

  @Autowired
  public SchemeService(SchemeRepository schemeRepository) {
    this.schemeRepository = schemeRepository;
  }

  public List<Scheme> findAll() throws NoSchemeFound {
    List<Scheme> result = new ArrayList<>();
    schemeRepository.findAll().forEach(result::add);
    if (result.isEmpty()) {
      throw new NoSchemeFound("No schemes found.");
    }
    return result;
  }

  public Scheme addScheme(Scheme scheme) throws SchemeAlreadyExists {
    return schemeRepository.save(scheme);
  }

  public void updateScheme(Scheme scheme) throws NoSchemeFound {
    if (!schemeRepository.existsById(scheme.getId())) {
      throw new NoSchemeFound("Scheme not found.");
    }
    schemeRepository.save(scheme);
  }

  public void deleteScheme(Scheme scheme) {
    schemeRepository.delete(scheme);
  }
}
