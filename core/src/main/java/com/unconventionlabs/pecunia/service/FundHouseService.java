package com.unconventionlabs.pecunia.service;

import com.unconventionlabs.pecunia.model.FundHouse;
import com.unconventionlabs.pecunia.repo.FundHouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FundHouseService {

    private final FundHouseRepository fundHouseRepository;

    @Autowired
    public FundHouseService(FundHouseRepository fundHouseRepository) {
        this.fundHouseRepository = fundHouseRepository;
    }

    public List<FundHouse> findAll() {
        List<FundHouse> result = new ArrayList<>();
        fundHouseRepository.findAll().forEach(result::add);
        return result;
    }

    public FundHouse findByName(String name) {
        return fundHouseRepository.findOneByName(name);
    }

    public void addFundHouse(FundHouse fundHouse) {
        fundHouseRepository.save(fundHouse);
    }
}
