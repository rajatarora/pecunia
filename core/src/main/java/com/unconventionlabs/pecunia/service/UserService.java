package com.unconventionlabs.pecunia.service;

import com.unconventionlabs.pecunia.model.PUser;
import com.unconventionlabs.pecunia.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<PUser> findAll() {
        List<PUser> result = new ArrayList<>();
        userRepository.findAll().forEach(result::add);
        return result;
    }

    public void addUser(PUser user) {
        userRepository.save(user);
    }
}
