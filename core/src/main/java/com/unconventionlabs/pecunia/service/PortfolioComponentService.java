package com.unconventionlabs.pecunia.service;

import com.unconventionlabs.pecunia.model.PortfolioComponent;
import com.unconventionlabs.pecunia.repo.PortfolioComponentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PortfolioComponentService {

    private final PortfolioComponentRepository portfolioComponentRepository;

    @Autowired
    public PortfolioComponentService(PortfolioComponentRepository portfolioComponentRepository) {
        this.portfolioComponentRepository = portfolioComponentRepository;
    }

    public List<PortfolioComponent> findAll() {
        List<PortfolioComponent> result = new ArrayList<>();
        portfolioComponentRepository.findAll().forEach(result::add);
        return result;
    }

    public List<PortfolioComponent> findAllByPortfolio(String portfolioName) {
        List<PortfolioComponent> result = new ArrayList<>();
        portfolioComponentRepository.findAllByPortfolio(portfolioName).forEach(result::add);
        return result;
    }
}
