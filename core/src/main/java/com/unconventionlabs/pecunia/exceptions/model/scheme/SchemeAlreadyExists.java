package com.unconventionlabs.pecunia.exceptions.model.scheme;

public class SchemeAlreadyExists extends SchemeException {

  public SchemeAlreadyExists() {
    super();
  }

  public SchemeAlreadyExists(String msg) {
    super(msg);
  }

  public SchemeAlreadyExists(Throwable th) {
    super(th);
  }

  public SchemeAlreadyExists(String msg, Throwable th) {
    super(msg, th);
  }

}
