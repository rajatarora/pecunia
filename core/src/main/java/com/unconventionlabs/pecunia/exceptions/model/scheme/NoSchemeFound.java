package com.unconventionlabs.pecunia.exceptions.model.scheme;

public class NoSchemeFound extends SchemeException {

  public NoSchemeFound() {
    super();
  }

  public NoSchemeFound(String msg) {
    super(msg);
  }

  public NoSchemeFound(Throwable th) {
    super(th);
  }

  public NoSchemeFound(String msg, Throwable th) {
    super(msg, th);
  }

}
