package com.unconventionlabs.pecunia.exceptions.model.portfoliocomponent;

public class PortfolioComponentNotFound extends PortfolioComponentException {

  public PortfolioComponentNotFound() {
    super();
  }

  public PortfolioComponentNotFound(String msg) {
    super(msg);
  }

  public PortfolioComponentNotFound(Throwable th) {
    super(th);
  }

  public PortfolioComponentNotFound(String msg, Throwable th) {
    super(msg, th);
  }

}
