package com.unconventionlabs.pecunia.exceptions.model.portfolio;

import com.unconventionlabs.pecunia.exceptions.model.ModelException;

public class PortfolioException extends ModelException {

  public PortfolioException() {
    super();
  }

  public PortfolioException(String msg) {
    super(msg);
  }

  public PortfolioException(Throwable th) {
    super(th);
  }

  public PortfolioException(String msg, Throwable th) {
    super(msg, th);
  }

}
