package com.unconventionlabs.pecunia.exceptions.model.fundhouse;

public class FundHouseAlreadyExists extends FundHouseException {

  public FundHouseAlreadyExists() {
    super();
  }

  public FundHouseAlreadyExists(String msg) {
    super(msg);
  }

  public FundHouseAlreadyExists(Throwable th) {
    super(th);
  }

  public FundHouseAlreadyExists(String msg, Throwable th) {
    super(msg, th);
  }

}
