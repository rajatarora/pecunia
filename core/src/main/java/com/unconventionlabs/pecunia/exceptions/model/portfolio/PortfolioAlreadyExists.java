package com.unconventionlabs.pecunia.exceptions.model.portfolio;

public class PortfolioAlreadyExists extends PortfolioException {

  public PortfolioAlreadyExists() {
    super();
  }

  public PortfolioAlreadyExists(String msg) {
    super(msg);
  }

  public PortfolioAlreadyExists(Throwable th) {
    super(th);
  }

  public PortfolioAlreadyExists(String msg, Throwable th) {
    super(msg, th);
  }

}
