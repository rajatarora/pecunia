package com.unconventionlabs.pecunia.exceptions.model.fundhouse;

import com.unconventionlabs.pecunia.exceptions.model.ModelException;

public class FundHouseException extends ModelException {

  public FundHouseException() {
    super();
  }

  public FundHouseException(String msg) {
    super(msg);
  }

  public FundHouseException(Throwable th) {
    super(th);
  }

  public FundHouseException(String msg, Throwable th) {
    super(msg, th);
  }

}
