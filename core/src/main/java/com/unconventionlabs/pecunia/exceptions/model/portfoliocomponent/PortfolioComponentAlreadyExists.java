package com.unconventionlabs.pecunia.exceptions.model.portfoliocomponent;

public class PortfolioComponentAlreadyExists extends PortfolioComponentException {

  public PortfolioComponentAlreadyExists() {
    super();
  }

  public PortfolioComponentAlreadyExists(String msg) {
    super(msg);
  }

  public PortfolioComponentAlreadyExists(Throwable th) {
    super(th);
  }

  public PortfolioComponentAlreadyExists(String msg, Throwable th) {
    super(msg, th);
  }

}
