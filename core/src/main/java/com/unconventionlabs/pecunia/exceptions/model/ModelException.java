package com.unconventionlabs.pecunia.exceptions.model;

public class ModelException extends Exception {

  public ModelException() {
    super();
  }

  public ModelException(String msg) {
    super(msg);
  }

  public ModelException(Throwable th) {
    super(th);
  }

  public ModelException(String msg, Throwable th) {
    super(msg, th);
  }

}
