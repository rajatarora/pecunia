package com.unconventionlabs.pecunia.exceptions.model.portfolio;

public class NoPortfolioFound extends PortfolioException {

  public NoPortfolioFound() {
    super();
  }

  public NoPortfolioFound(String msg) {
    super(msg);
  }

  public NoPortfolioFound(Throwable th) {
    super(th);
  }

  public NoPortfolioFound(String msg, Throwable th) {
    super(msg, th);
  }

}
