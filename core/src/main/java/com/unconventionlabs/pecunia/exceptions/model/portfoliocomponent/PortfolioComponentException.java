package com.unconventionlabs.pecunia.exceptions.model.portfoliocomponent;

import com.unconventionlabs.pecunia.exceptions.model.ModelException;

public class PortfolioComponentException extends ModelException {

  public PortfolioComponentException() {
    super();
  }

  public PortfolioComponentException(String msg) {
    super(msg);
  }

  public PortfolioComponentException(Throwable th) {
    super(th);
  }

  public PortfolioComponentException(String msg, Throwable th) {
    super(msg, th);
  }

}
