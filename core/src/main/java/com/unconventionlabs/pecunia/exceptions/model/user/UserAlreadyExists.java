package com.unconventionlabs.pecunia.exceptions.model.user;

public class UserAlreadyExists extends UserException {

  public UserAlreadyExists() {
    super();
  }

  public UserAlreadyExists(String msg) {
    super(msg);
  }

  public UserAlreadyExists(Throwable th) {
    super(th);
  }

  public UserAlreadyExists(String msg, Throwable th) {
    super(msg, th);
  }

}
