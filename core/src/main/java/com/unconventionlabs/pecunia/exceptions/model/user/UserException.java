package com.unconventionlabs.pecunia.exceptions.model.user;

import com.unconventionlabs.pecunia.exceptions.model.ModelException;

public class UserException extends ModelException {

  public UserException() {
    super();
  }

  public UserException(String msg) {
    super(msg);
  }

  public UserException(Throwable th) {
    super(th);
  }

  public UserException(String msg, Throwable th) {
    super(msg, th);
  }

}
