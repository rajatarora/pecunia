package com.unconventionlabs.pecunia.exceptions.model.user;

public class NoUserFound extends UserException {

  public NoUserFound() {
    super();
  }

  public NoUserFound(String msg) {
    super(msg);
  }

  public NoUserFound(Throwable th) {
    super(th);
  }

  public NoUserFound(String msg, Throwable th) {
    super(msg, th);
  }

}
