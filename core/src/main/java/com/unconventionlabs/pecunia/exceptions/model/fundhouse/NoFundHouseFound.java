package com.unconventionlabs.pecunia.exceptions.model.fundhouse;

public class NoFundHouseFound extends FundHouseException {

  public NoFundHouseFound() {
    super();
  }

  public NoFundHouseFound(String msg) {
    super(msg);
  }

  public NoFundHouseFound(Throwable th) {
    super(th);
  }

  public NoFundHouseFound(String msg, Throwable th) {
    super(msg, th);
  }

}
