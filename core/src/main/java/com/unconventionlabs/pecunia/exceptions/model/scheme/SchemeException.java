package com.unconventionlabs.pecunia.exceptions.model.scheme;

import com.unconventionlabs.pecunia.exceptions.model.ModelException;

public class SchemeException extends ModelException {

  public SchemeException() {
    super();
  }

  public SchemeException(String msg) {
    super(msg);
  }

  public SchemeException(Throwable th) {
    super (th);
  }

  public SchemeException(String msg, Throwable th) {
    super(msg, th);
  }

}
