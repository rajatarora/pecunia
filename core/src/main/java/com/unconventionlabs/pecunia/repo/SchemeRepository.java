package com.unconventionlabs.pecunia.repo;

import com.unconventionlabs.pecunia.model.Scheme;
import org.springframework.data.repository.CrudRepository;

public interface SchemeRepository extends CrudRepository<Scheme, Long> {
}
