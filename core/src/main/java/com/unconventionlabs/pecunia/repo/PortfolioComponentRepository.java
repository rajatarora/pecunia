package com.unconventionlabs.pecunia.repo;

import com.unconventionlabs.pecunia.model.PortfolioComponent;
import org.springframework.data.repository.CrudRepository;

public interface PortfolioComponentRepository extends CrudRepository<PortfolioComponent, Long> {

    Iterable<PortfolioComponent> findAllByPortfolio(String portfolioName);

}
