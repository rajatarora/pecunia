package com.unconventionlabs.pecunia.repo;

import com.unconventionlabs.pecunia.model.Portfolio;
import org.springframework.data.repository.CrudRepository;

public interface PortfolioRepository extends CrudRepository<Portfolio, Long> {

}
