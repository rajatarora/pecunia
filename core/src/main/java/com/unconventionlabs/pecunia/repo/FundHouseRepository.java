package com.unconventionlabs.pecunia.repo;

import com.unconventionlabs.pecunia.model.FundHouse;
import org.springframework.data.repository.CrudRepository;

public interface FundHouseRepository extends CrudRepository<FundHouse, Long> {

    FundHouse findOneByName(String name);
}
