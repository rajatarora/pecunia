package com.unconventionlabs.pecunia.repo;

import com.unconventionlabs.pecunia.model.PUser;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<PUser, String> {

}
