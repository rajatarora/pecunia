package com.unconventionlabs.pecunia.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Scheme {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private String isin;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String amfiId;

  @ManyToOne
  private FundHouse fundHouse;

  private SchemeType schemeType;

}
