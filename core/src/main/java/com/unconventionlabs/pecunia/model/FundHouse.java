package com.unconventionlabs.pecunia.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class FundHouse {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany
    private List<Scheme> schemes;
}
