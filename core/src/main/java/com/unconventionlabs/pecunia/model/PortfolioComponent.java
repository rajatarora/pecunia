package com.unconventionlabs.pecunia.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Data
@Entity
public class PortfolioComponent {

    @Id
    @GeneratedValue
    private Long id;

    private String scheme;

    private Double units;

    private LocalDate purchaseDate;

    private Double unitPrice;

    @ManyToOne
    private Portfolio portfolio;
}
