package com.unconventionlabs.pecunia.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class PUser {

    @Id
    private String userName;

    private String email;

    private String firstName;

    private String lastName;

    @OneToMany
    private List<Portfolio> portfolios;

}
