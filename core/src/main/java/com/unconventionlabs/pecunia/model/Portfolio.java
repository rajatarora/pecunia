package com.unconventionlabs.pecunia.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Portfolio {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToOne
    private PUser user;

    @OneToMany
    private List<PortfolioComponent> components;
}
