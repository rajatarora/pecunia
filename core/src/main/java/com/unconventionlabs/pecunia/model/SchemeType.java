package com.unconventionlabs.pecunia.model;

public enum SchemeType {
  DIVIDEND,
  GROWTH
}
