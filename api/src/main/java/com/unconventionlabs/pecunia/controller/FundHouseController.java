package com.unconventionlabs.pecunia.controller;

import com.unconventionlabs.pecunia.model.FundHouse;
import com.unconventionlabs.pecunia.service.FundHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FundHouseController {

    private final FundHouseService fundHouseService;

    @Autowired
    public FundHouseController(FundHouseService fundHouseService) {
        this.fundHouseService = fundHouseService;
    }

    @RequestMapping(value = "/fundhouses", method = RequestMethod.GET)
    public List<FundHouse> getAllFundHouses() {
        return fundHouseService.findAll();
    }

    @RequestMapping(value = "/fundhouse", method = RequestMethod.POST)
    public void addFundHouse(@RequestBody FundHouse fundHouse) {
        fundHouseService.addFundHouse(fundHouse);
    }
}
