package com.unconventionlabs.pecunia.controller;

import com.unconventionlabs.pecunia.exceptions.model.scheme.NoSchemeFound;
import com.unconventionlabs.pecunia.exceptions.model.scheme.SchemeAlreadyExists;
import com.unconventionlabs.pecunia.model.Scheme;
import com.unconventionlabs.pecunia.service.SchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SchemeController {

  private final SchemeService schemeService;

  @Autowired
  public SchemeController(SchemeService schemeService) {
    this.schemeService = schemeService;
  }

  @RequestMapping(value = "/schemes", method = RequestMethod.GET)
  public List<Scheme> getAllSchemes() throws NoSchemeFound {
    return schemeService.findAll();
  }

  @RequestMapping(value = "/scheme", method = RequestMethod.POST)
  public ResponseEntity<Scheme> addScheme(@RequestBody Scheme scheme) throws SchemeAlreadyExists {
    return new ResponseEntity<>(schemeService.addScheme(scheme), HttpStatus.CREATED);
  }
}
